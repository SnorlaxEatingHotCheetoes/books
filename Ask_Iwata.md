# Ask Iwata
 
* Author: Satoru Iwata (Edited by Hobonichi) 
* ISBN: 978-1-9747-2154-2

## Takeaways
* Page 15

    `If you let people tell you what they want to say, and wait for them to finish, they'll give you a chance to weight in afterward. I believe that whether people decide to accept what they are told or not is entirely based on whether they think 'Your'e only saying this to benefit yourself' or 'You're saying this because, deep down, you believe it.'"  This is why I think that management depends on an ability to minimize your personal agenda.`

* Page 32

    `We made it absolutely clear that our mission was to "shock people, in a good way." Unless you can shock people, you'll never gain new customers.`

* Page 33
    
    `The most important aspect of a job interview: I've always wondered why most interviews lead with the hardest questions. Why can't they lead with the easiest questions? In my experience, there are two types of interviewers: those who make a person feel at ease in order to get a sense of who they really are and judge their candidcay accordingly, and those who believe an unrelaxed person, though unable to speak their mind, will reveal all kinds of things about themselves, like how sociable or strong they are. I'm the first type. My belief is that the second type of interviewere is able to glimpse only a part of somebody's potential. Until you've let a person be themselves, the conersation hasn't really gotten started.`

* Page 36-37

    `Work is tough, full of unpleasant tasks. Some degree of perserverance is essential. Still, I suspect that wheter or not a job is fun for someone depends a great deal on the breadth of their idea of what they're able to enjoy. Depending on how you approach it, work can feel dull. But, if you're able to find the fun in discovering new things, almost everything you do can become interesting. This realization ca be a major turning point in enjoying your job.`

* Page 37

    `So how do you know when a project is going well? When someone points to a gray area in the initial plan, then asks you "Hey, can I take care of this?" and follows through. Projects where this keeps on happening tend to end up going well. On the other hand, when this phenomonon fails to occur, you might finish up all right, but things will feel a bit dysfunctional, which is never a good sign.`

* Page 52

    `When the reward feels like it's greater than the energy and effort we've expended, we don't give up. But when the result of all our efforts is a disappointment, we tend to fail.`

* Page 52-53

    `Talent basically involves the ability to find rewards. In my view, talent isn't about achieving results so much as deriving pleasure from the results that you've achieved. When somebody has tapped into their stores of talent, the cycle of discovery and reward is up and running.`

* Page 56

    `But regardless of whether I like it or not, once I'm convinced that I'm the best person for the job, I'm going to step up to the plate.`

* Page 57

    `For every task I tacked with resolve, there are tons of things that I stay out of because I don't actually need to be involved. That way, I can focus on the things that only I can do.`

* Page 58

    `A while back, I went on record saying "Programmers should never say no." When you're making games, if a programmer says, "That's impossible," it not only puts the brakes on a valuable idea but makes it harder for the next idea to come. If programmers only focus on things that are easy to program, they'll never break the mold and come up with fascinating ideas. Besides, through the process of trial and error, all kinds of things that seem impossible at first can wind up being a success.`

* Page 60-61
    
    `After all, the time you spend thinking about totally random stuff is never wasted. Rather, it's more about deciding where to direct your limited supply of time and energy. On a deeper level, I think this is about doing what you were born to do. Either way, spreading yourself thin will get you nowhere. Same goes for companies. If you let the masses dictate your decision-making, you're forced to cast a wide net, and you won't be able to give individual projects the attention they deserve. As a result, the work lacks depth, and above all, has no secondary gains. The best bet, both on the inidividual level and the corporate level is to take stock of your abilities and assign priority to minimize regret. Regret is part of life, but it's something we would all rather avoid. If we can even slightly dial back how often we tell ourselves, "I wish I'd done things differently," I think we'd be a whole lot happier.`

* Page 69

    `"A good idea is something that solves multiple problems in a flash."`

* Page 75

    `As a general rule, I'd rather play to my strengths than do something I'm bad at.`

* Page 102-103

    `Though speaking personally, I think nothing is more hazardous than staying the course. Nobody can say for sure when to make a change. When Nintendo takes a different tack, there's no telling whether it will take one year, or two, or three, or even five for people to catch on. But staying the course meants not having a future. If you maintain the status quo, you wind up fighting for survival, and gradually your fan base disappears. That's the one direction I'm always trying to avoid. There's always the question of how far you can shift and still have people understand and follow along. However forging straight ahead will get you nowhere. I recognized that plugging away down a path that had no future, toward oblivioin, would mean all of our effort was in vain, and knowing this steeled my resolve.`

* Page 127

    `Iwata didn't read in search of hints on how to do things, but to find support for the ideas that constituted his worldview, and so that he could use books as a resource for conveying these ideas to others.1`

 
